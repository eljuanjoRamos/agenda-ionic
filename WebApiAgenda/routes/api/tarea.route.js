var express = require('express');
var tarea = require('../../model/tarea.model');
var services = require('../../services');
var router = express.Router();


router.get('/tarea/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  tarea.selectAll(idUsuario, function(error, resultados){
        if(typeof resultados !== undefined) {
          res.json(resultados);
        } else {
          res.json({"Mensaje": "No hay tareas"});
        }
      });
    
});


router.get('/tarea/:id', services.verificar, function(req, res, next) {
  var idTarea = req.params.id;
  tarea.select(idTarea, function(resultado) {
    if(typeof resultado !== 'undefined') {
      res.json(resultado.find(c => c.idTarea == idTarea));
    } else {
      res.json({"mensaje" : "No hay tareas"});
    }
  });
});


router.post('/tarea', services.verificar, function(req, res, next) {
  var id = req.usuario.idUsuario;
  var data = {
    titulo : req.body.titulo,
    descripcion : req.body.descripcion,
    fechaEntrega : req.body.fechaEntrega,
    estado : req.body.estado
  };

  tarea.insert(id, data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego la Tarea"
      });
    } else {
      res.json({"mensaje":"No se ingreso la Tarea"});
    }
  });
});

router.put('/tarea/:id', services.verificar, function(req, res, next){
  var id = req.params.id;
  var data = {
    idTarea : id,
    titulo : req.body.titulo,
    descripcion : req.body.descripcion,
    fechaEntrega : req.body.fechaEntrega,
    estado : req.body.estado,
    idUsuario : req.usuario.idUsuario
  }
  tarea.update(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });
});
router.delete('/tarea/:id', services.verificar, function(req, res, next){
  var idC = req.params.id;
  var idU = req.usuario.idUsuario;
  var data = {
    idUsuario: idU,
    idContacto: idC
  }
  tarea.delete(idC, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino la tarea correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino la tarea"});
    }
  });
});

module.exports = router;
