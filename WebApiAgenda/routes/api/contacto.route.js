var express = require('express');
var contacto = require('../../model/contacto.model');
var services = require('../../services');
var router = express.Router();


router.get('/contacto/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
      contacto.selectAll(idUsuario, function(error, resultados){
        if(typeof resultados !== undefined) {
          res.json(resultados);
        } else {
          res.json({"Mensaje": "No hay contactos"});
        }
      });

});

router.get('/historial/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
      contacto.selectHistorial(idUsuario, function(error, resultados){
        if(typeof resultados !== undefined) {
          res.json(resultados);
        } else {
          res.json({"Mensaje": "No hay contactos"});
        }
      });
});

router.get('/contacto/:id', services.verificar, function(req, res, next) {
  var idContacto = req.params.id;
  contacto.select(idContacto, function(contactos) {
    if(typeof contactos !== 'undefined') {
      res.json(contactos.find(c => c.idContacto == idContacto));
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});

router.post('/contacto', services.verificar, function(req, res, next) {
  var id = req.usuario.idUsuario; 
  var data = {
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    direccion : req.body.direccion,
    telefono : req.body.telefono,
    correo: req.body.correo,
    genero: req.body.genero,
    idCategoria : req.body.idCategoria
  };
  contacto.insert(data, id, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego el contacto"
      });
    } else {
      res.json({"mensaje":"No se ingreso el contacto"});
    }
  });
});

router.put('/contacto/:idContacto', services.verificar, function(req, res, next){
  var c = req.params.idContacto;
  
  var data = {
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    direccion : req.body.direccion,
    telefono : req.body.telefono,
    correo: req.body.correo,
    genero: req.body.genero,
    idCategoria : req.body.idCategoria,
    idContacto : c,
    idUsuario: req.usuario.idUsuario
  }
  
  contacto.update(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });
});


router.delete('/contacto/:id', services.verificar, function(req, res, next){
  var idC = req.params.id;
  var idU = req.usuario.idUsuario;
  var data = {
    idUsuario: idU,
    idContacto: idC
  }
  console.log(idC);
  console.log(data);
  contacto.delete(data, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino el contacto correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino el contacto"});
    }
  });
});



module.exports = router;
