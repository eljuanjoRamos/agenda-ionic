var express = require('express');
var router = express.Router();
var categoria = require('../../model/categoria.model');
var services = require('../../services');

router.get('/categoria/', services.verificar,  function(req, res, next) {
  categoria.selectAll(function(error, resultados) {
    if(typeof resultados !== 'undefined') {
      res.json(resultados);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});
router.get('/categoria/:id', services.verificar, function(req, res, next) {
  var idCategoria = req.params.id;
  categoria.select(idCategoria, function(categorias) {
    if(typeof categorias !== 'undefined') {
      res.json(categorias.find(c => c.idCategoria == idCategoria));
    } else {
      res.json({"mensaje" : "No hay contactos"});
    }
  });
});

router.post('/categoria', services.verificar, function(req, res, next) {
  var nombre = req.body.nombreCategoria;

  categoria.insert(nombre, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego la Categoria"
      });
    } else {
      res.json({"mensaje":"No se ingreso la categoria"});
    }
  });
});

router.put('/categoria/:id', services.verificar, function(req, res, next){
  var c = req.params.id;
  
  var data = {
    nombreCategoria : req.body.nombreCategoria,
    idCategoria: c
  }
  
  categoria.update(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });
});


router.delete('/categoria/:id', services.verificar, function(req, res, next){
  var idC = req.params.id;
  categoria.delete(idC, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino"});
    }
  });
});

module.exports = router;
