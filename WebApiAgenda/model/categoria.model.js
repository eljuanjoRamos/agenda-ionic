var database = require("../config/database.config");
var Categoria = {};


Categoria.selectAll = function(callback) {
  if(database) {
		var query = "SELECT * FROM Categoria WHERE idCategoria !=1";
    database.query(query,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}


Categoria.select = function(id, callback) {
  console.log(id);
	if(database) {
		var query = "SELECT * FROM Categoria WHERE idCategoria = ?";
    database.query(query, id,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });
  }
}//

Categoria.insert = function(data, callback) {
  if(database) {
    database.query('CALL SP_InsertarCategoria(?)', data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Categoria.update = function(data, callback){
	if(database) {
		database.query('CALL SP_ActualiarCategoria(?,?)',
		[data.nombreCategoria, data.idCategoria],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback({"affectedRows": resultado.affectedRows});
			}
		});
	}
}

Categoria.delete = function(idCategoria, callback) {
	if(database) {
		database.query('CALL SP_BorrarCategoria(?)', idCategoria,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Categoria;
