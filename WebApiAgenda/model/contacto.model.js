var database = require("../config/database.config");
var Contacto = {};


Contacto.selectAll = function(idUsuario, callback) {
  if(database) {
		var query = "SELECT contacto.idContacto, nombre, apellido, direccion, telefono, correo, genero, nombreCategoria FROM usuarioDetalle INNER JOIN contacto on usuarioDetalle.idContacto = contacto.idContacto INNER JOIN categoria ON contacto.idCategoria = categoria.idCategoria  where idUsuario = ?";
    database.query(query, idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}


Contacto.selectHistorial = function(idUsuario, callback) {
  if(database) {
		var query = "SELECT * FROM Historial where idUsuario = ?";
    database.query(query, idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}

Contacto.select = function(id, callback) {
	if(database) {
		var query = "SELECT * FROM Contacto  INNER JOIN categoria ON contacto.idCategoria = categoria.idCategoria  where idContacto = ?";
    database.query(query, id,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });
  }
}
Contacto.insert = function(data, idUsuario, callback) {
  if(database) {
    database.query('CALL SP_InsertarContactoUsuario(?,?,?,?,?,?,?,?)',
    [data.nombre, data.apellido, data.direccion, data.telefono, data.correo,data.genero, data.idCategoria, idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Contacto.update = function(data, callback){
	if(database) {
		database.query('CALL SP_ActualizarContacto(?,?,?,?,?,?,?,?,?)',
		[data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.genero, data.idCategoria, data.idContacto, data.idUsuario],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback({"affectedRows": resultado.affectedRows});
			}
		});
	}
}

Contacto.delete = function(id, callback) {
	if(database) {
		database.query('CALL SP_BorrarContactoUsuario(?,?)', [id.idUsuario, id.idContacto],
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}


module.exports = Contacto;
