var database = require("../config/database.config");
var tarea = {};


tarea.selectAll = function(id, callback) {
  if(database) {
    database.query("SELECT idTarea, titulo, descripcion, fechaRegistro, fechaEntrega, estado, nick FROM Tareas INNER JOIN Usuario ON tareas.idUsuario = usuario.idUsuario WHERE tareas.idUsuario = ?",id,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}
tarea.select = function(id, callback) {
	if(database) {
		var query = "SELECT * FROM tareas  INNER JOIN usuario ON tareas.idUsuario = usuario.idUsuario  where idTarea = ?";
    database.query(query, id,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });//Fin query
  }//Fin IF
}





tarea.insert = function(idUsuario, data, callback) {
  if(database) {
    database.query("CALL SP_InsertarTarea(?,?,?,?,?)", [data.titulo, data.descripcion, data.fechaEntrega, data.estado, idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}


tarea.update = function(data, callback) {
  if(database) {
    var sql = "CALL SP_ActualizarTarea(?,?,?,?,?,?)";
    database.query(sql, [data.titulo, data.descripcion, data.fechaEntrega, data.estado, data.idUsuario, data.idTarea],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });    
  }
}

tarea.delete = function(id, callback) {
	if(database) {
		database.query('DELETE FROM Tareas WHERE idTarea = ?', id,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}
module.exports = tarea;
