var database = require("../config/database.config");
var Cita = {};


Cita.selectAll = function(idUsuario, callback) {
  if(database) {
	var query = "SELECT idCita, asunto, detalles, fecha, lugar, contacto.idContacto, nombre, apellido FROM Cita INNER JOIN Contacto ON Cita.idContacto = contacto.idContacto WHERE idUsuario = ?";
    database.query(query, idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        console.log(JSON.stringify(resultados));
        callback(null, resultados);
      }
    });
  }
}

Cita.select = function(id, callback) {
	if(database) {
		var query = "SELECT idCita, asunto, detalles, fecha, lugar, contacto.idContacto, nombre, apellido FROM Cita  INNER JOIN contacto ON cita.idContacto = contacto.idContacto  where idCita = ?";
    database.query(query, id,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });
  }
}
Cita.insert = function(data, callback) {
  if(database) {
    database.query('CALL SP_AgregarCita(?,?,?,?,?,?)',
    [data.asunto, data.detalles, data.fecha, data.lugar, data.idUsuario, data.idContacto],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Cita.update = function(data, callback){
	if(database) {
		database.query('UPDATE Cita SET asunto = ?, detalles = ?, fecha = ?, lugar = ?, idUsuario = ?, idContacto = ? WHERE idCita = ?',
		[data.asunto, data.detalles, data.fecha, data.lugar, data.idUsuario, data.idContacto, data.idCita],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback({"affectedRows": resultado.affectedRows});
			}
		});
	}
}

Cita.delete = function(id, callback) {

	if(database) {
		database.query('DELETE FROM Cita WHERE idCita = ?', id,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}


module.exports = Cita;
