var database = require("../config/database.config");
var usuario = {};

usuario.login = function(data, callback) {
  if(database) {
    var consulta = 'CALL SP_Autenticar(?, ?);';
		database.query(consulta, [data.nick, data.contrasena], function(error, resultado){
			if(error) {
				throw error;
			} else {
				if(resultado[0].length > 0) {
					callback(resultado[0]);	
				} else {
					callback(0);
				}
			}
		});
	}
}


usuario.selectAll = function(callback) {
	if(database) {
		var consulta = 'SELECT * FROM Usuario';
		database.query(consulta, function(error, resultado){
			if(error) throw error;
			callback(resultado);
		});
	}
}
usuario.insert = function(data, callback) {
  if(database) {
    database.query('CALL SP_InsertarUsuario(?,?)',
    [data.nick, data.contrasena],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}


usuario.update = function(data, callback){
	if(database) {
		database.query('CALL SP_ActualizarUsuario(?,?,?)',
		[data.nick, data.contrasena, data.idUsuario],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback({"affectedRows": resultado.affectedRows});
			}
		});
	}
}
usuario.delete = function(idUsuario, callback) {
	if(database) {
		database.query('DELETE FROM Usuario WHERE idUsuario = ?', idUsuario,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = usuario;
