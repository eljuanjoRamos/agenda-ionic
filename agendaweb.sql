CREATE DATABASE AgendaWeb1;

USE AgendaWeb1;

CREATE TABLE Usuario(
   idUsuario INT AUTO_INCREMENT NOT NULL,
   nick VARCHAR(30) NOT NULL,
   contrasena VARCHAR(30) NOT NULL,
   PRIMARY KEY(idUsuario)
);

CREATE TABLE Categoria(
  idCategoria INT AUTO_INCREMENT NOT NULL,
  nombreCategoria VARCHAR(30) NOT NULL,
  PRIMARY KEY(idCategoria)
);

CREATE TABLE Contacto(
  idContacto INT AUTO_INCREMENT NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  apellido VARCHAR(30) NOT NULL,
  direccion VARCHAR(30) NOT NULL,
  telefono VARCHAR(12) NOT NULL,
  correo VARCHAR(40) NOT NULL,
  genero VARCHAR(40) NOT NULL,
  idCategoria INT NOT NULL,
  PRIMARY KEY(idContacto),
  FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria) ON DELETE CASCADE
  );


CREATE TABLE Historial(
   idHistorial INT AUTO_INCREMENT NOT NULL,
   idUsuario INT NOT NULL,
   fecha DATETIME NOT NULL,
   cambio VARCHAR(255) NOT NULL,
   PRIMARY KEY(idHistorial),
   FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario) ON DELETE CASCADE
);

CREATE TABLE UsuarioDetalle(
   idUsuarioDetalle INT AUTO_INCREMENT NOT NULL,
   idUsuario INT NOT NULL,
   idContacto INT NOT NULL,
   PRIMARY KEY(idUsuarioDetalle),
   FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario) ON DELETE CASCADE,
   FOREIGN KEY(idContacto) REFERENCES Contacto(idContacto) ON DELETE CASCADE
);

CREATE TABLE Tareas(
	idTarea INT AUTO_INCREMENT NOT NULL,
    titulo VARCHAR(255) NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    fechaRegistro DATETIME NOT NULL,
    fechaEntrega VARCHAR(255) NOT NULL,
    estado VARCHAR(255) NOT NULL,
    idUsuario INT NOT NULL,
    PRIMARY KEY(idTarea),
    FOREIGN KEY(idUsuario) REFERENCES usuario(idUsuario) ON DELETE CASCADE
);
CREATE TABLE Cita(
    idCita INT AUTO_INCREMENT NOT NULL,
    asunto VARCHAR(255) NOT NULL,
    detalles VARCHAR(255) NOT NULL,
    fecha VARCHAR(255) NOT NULL,
    lugar VARCHAR(255) NOT NULL,
    idUsuario INT NOT NULL,
    idContacto INT NOT NULL,
    PRIMARY KEY(idCita),
    FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario) ON DELETE CASCADE,
    FOREIGN KEY(idContacto) REFERENCES Contacto(idContacto) ON DELETE CASCADE
);

-- PROCEDIMIENTOS ALMACENADOS

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_InsertarUsuario` (IN `nick1` VARCHAR(30), IN `contrasena1` VARCHAR(30))  BEGIN
	INSERT INTO Usuario(nick, contrasena) VALUES(nick1, contrasena1);
END$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ActualizarUsuario` (IN `nick1` VARCHAR(30), IN `contrasena1` VARCHAR(30), IN `idUsuario1` INT)  BEGIN
	insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se Edito su cuenta de ", (SELECT Concat(nick," y ",contrasena) FROM usuario where idUsuario = idUsuario1), " a ", (Concat(nick1, " y ", contrasena1))));
    UPDATE Usuario SET nick = nick1, contrasena =  contrasena1 WHERE idUsuario = idUsuario1;
END$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ActualizarContacto` (IN `nombre1` VARCHAR(30), IN `apellido1` VARCHAR(30), IN `direccion1` VARCHAR(30), IN `telefono1` VARCHAR(12), IN `correo1` VARCHAR(40), IN `genero1` VARCHAR(40), IN `idCategoria1` INT, IN `idContacto1` INT, IN `idUsuario1` INT)  BEGIN
	insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se modifico el contacto ", (SELECT Concat(nombre," ",apellido) FROM contacto where idContacto = idContacto1), " por ", Concat(nombre1, " ", apellido1)));
    UPDATE Contacto SET nombre = nombre1, apellido = apellido1, direccion = direccion1, telefono = telefono1, correo = correo1, genero = genero1, idCategoria = idCategoria1 WHERE idContacto = idContacto1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_BorrarContacto` (IN `idContacto1` INT)  BEGIN
	insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se Elimino el contacto ", (SELECT Concat(nombre," ",apellido) FROM contacto where idContacto = idContacto1)));
    DELETE FROM Contacto WHERE idContacto = idContacto1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_BorrarContactoUsuario` (IN `idUsuario1` INT, IN `idContacto1` INT)  BEGIN
	insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se elimino el contacto ", (SELECT Concat(nombre," ",apellido) FROM contacto where idContacto = idContacto1)));
	DELETE FROM Contacto WHERE idContacto = idContacto1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_InsertarCategoria` (IN `nombre` VARCHAR(30))  BEGIN
	INSERT INTO Categoria(nombreCategoria) VALUES(nombre);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ActualiarCategoria` (IN `nombre` VARCHAR(30), IN `idCategoria1` INT)  BEGIN
    UPDATE Categoria SET nombreCategoria = nombre WHERE idCategoria = idCategoria1;
END$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_BorrarCategoria` (IN `idCategoria1` INT)  BEGIN
	UPDATE Contacto SET idCategoria = 1 WHERE idCategoria = idCategoria1;
    DELETE FROM Categoria WHERE idCategoria = idCategoria1;
END$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_InsertarContactoUsuario` (IN `nombre1` VARCHAR(30), IN `apellido1` VARCHAR(30), IN `direccion1` VARCHAR(30), IN `telefono1` VARCHAR(12), IN `correo1` VARCHAR(40), IN `genero1` VARCHAR(40), IN `idCategoria1` INT, IN `idUsuario1` INT)  BEGIN
	INSERT INTO contacto (nombre, apellido, direccion, telefono, correo, genero, idCategoria) VALUES(nombre1, apellido1, direccion1, telefono1, correo1, genero1, idCategoria1);
    insert into usuarioDetalle(idUsuario, idContacto) values(idUsuario1, LAST_INSERT_ID());
	insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se agrego el contacto ", (SELECT Concat(nombre1," ",apellido1))));
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_InsertarTarea` (IN `titulo1` VARCHAR(255), IN `descripcion1` VARCHAR(255), IN `fechaEntrega1` VARCHAR(255), IN `estado1` VARCHAR(255), IN `idUsuario1` INT)
BEGIN
	INSERT INTO tareas (titulo, descripcion, fechaRegistro, fechaEntrega, estado, idUsuario) VALUES(titulo1, descripcion1, SYSDATE(), fechaEntrega1, estado1, idUsuario1);
    insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se agrego la tarea ", titulo1));
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_EliminarTarea` (IN `id` INT)
BEGIN
  insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se elimino la tarea ", (SELECT titulo FROM Tareas WHERE idTarea = idTarea1)));	
  DELETE FROM Tareas WHERE idTarea = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ActualizarTarea` (IN `titulo1` VARCHAR(255), IN `descripcion1` VARCHAR(255), IN `fechaEntrega1` VARCHAR(255), IN `estado1` VARCHAR(255), IN `idUsuario1` INT, IN `idTarea1` INT)
BEGIN
    insert into historial(idUsuario, fecha, cambio) values(idUsuario1, SYSDATE(), CONCAT("Se modifico la tarea ", (SELECT titulo FROM Tareas WHERE idTarea = idTarea1), " por ", titulo1));
	UPDATE Tareas SET titulo = titulo1, descripcion = descripcion1, fechaRegistro = SYSDATE(), fechaEntrega = fechaEntrega1, estado = estado1, idUsuario = idUsuario1 WHERE idTarea = idTarea1;
END$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_BuscarCategoria` (IN `id` INT)
BEGIN
  SELECT * FROM Categoria WHERE idCategoria = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_VerContactos` (in `id` INT)
BEGIN
	SELECT contacto.idContacto, nombre, apellido, direccion, telefono, correo, genero, nombreCategoria FROM usuarioDetalle INNER JOIN contacto on usuarioDetalle.idContacto = contacto.idContacto INNER JOIN categoria ON contacto.idCategoria = categoria.idCategoria  where idUsuario = id;
END$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_AgregarCita` (IN `asunto1` VARCHAR(255), IN `detalles1` VARCHAR(255), IN `fecha1` VARCHAR(255), IN `lugar1` VARCHAR(255), IN `idUsuario1` INT, IN `idContacto1` INT) 
 BEGIN
	INSERT INTO Cita(asunto, detalles, fecha, lugar, idUsuario, idContacto) VALUES(asunto1, detalles1, fecha1, lugar1, idUsuario1, idContacto1);
END$$


CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_VerCitas` (IN `idUsuario1` INT) 
 BEGIN
    SELECT idCita, asunto, detalles, fecha, lugar, contacto.idContacto, nombre, apellido FROM Cita INNER JOIN Contacto ON Cita.idContacto = contacto.idContacto WHERE idUsuario = idUsuario1;
END$$

CREATE  PROCEDURE `SP_Autenticar` (IN `nick1` VARCHAR(30), IN `contra` VARCHAR(30)) 
 BEGIN
    SELECT * FROM Usuario WHERE nick = nick1 AND contrasena = contra;
END$$

DELIMITER ;

CALL SP_InsertarCategoria('Otros');
CALL SP_InsertarCategoria('Familia');
CALL SP_InsertarCategoria('Amigos');
CALL SP_InsertarCategoria('Trabajo');
CALL SP_InsertarCategoria('Universidad');
CALL SP_InsertarCategoria('Escuela');
