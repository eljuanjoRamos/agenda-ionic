import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AuthService } from '../../app/services/auth.service';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignUpPage {
  private usuario:any = {
    nick:"",
    contrasena: ""
  }
  constructor(
    public navCtrl: NavController,
    public toast: ToastController,
    public auth: AuthService
  ) {
  }

  public registrarse() {
    this.auth.registrar(this.usuario)
    .subscribe(res => {
      this.toast.create({
        message: res.mensaje,
        duration: 2000
      }).present();

      if(res.estado) {
        setTimeout(() => {
          this.navCtrl.setRoot(TabsPage);
        }, 3000);
      }

    });
  }
}
