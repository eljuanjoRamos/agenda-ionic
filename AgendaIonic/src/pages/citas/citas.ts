import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CitaService } from '../../app/services/cita.service';
import { CitasFormPage } from './citas-form';

@Component({
  selector: 'page-citas',
  templateUrl: 'citas.html'
})
export class CitasPage {
  private citas:any[] = [];

  constructor(
    public navCtrl: NavController,
    public citaService: CitaService
  ) {
    this.inicializar();
  }
  private inicializar() {
    this.citaService.obtenerCita()
    .subscribe(citas => this.citas = citas);
  }
  public verForm(parametro) {
    this.navCtrl.push(CitasFormPage, {parametro});
  }

}
