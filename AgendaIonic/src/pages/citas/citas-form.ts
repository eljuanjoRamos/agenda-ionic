import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { ContactoService } from '../../app/services/contacto.service';
import { CitaService } from '../../app/services/cita.service';
import { CitasPage } from './citas';


@Component({
  selector: 'page-citas-form',
  templateUrl: 'citas-form.html'
})
export class CitasFormPage {
  private citas:any = {
    asunto: "",
    detalles: "",
    fecha: "",
    lugar: "",
    idUsuario: 0,
    idContacto: 0
  };
  private contacto:any[] = []
  private parametro:string;
  private encabezado:string;
  private texto:string;

  private inicializarContacto() {
    this.contactoService.obtenerContactos()
    .subscribe(contacto => this.contacto = contacto);
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public contactoService: ContactoService,
    public citaService: CitaService
  ) {
    this.parametro = this.navParams.get('parametro');
    if(this.parametro != 'nuevo') {
      this.encabezado = "Detalle de Cita";
      this.texto = "Datos Actuales de la cita";
      this.inicializarContacto();
      this.citaService.buscarCita(this.parametro)
      .subscribe(cita => this.citas = cita);
    } else {
      this.encabezado = "Nueva Cita";
      this.texto = "Nuevos Datos";
      this.inicializarContacto();
    }
  }

  public guardar() {
    this.citaService.nuevaCita(this.citas)
    .subscribe(res => {
      this.toast.create({
        message: res.mensaje,
        duration: 2000
      }).present();
      if(res.estado) {
        this.navCtrl.push(CitasPage);
      } else {
        this.citas.asunto = "";
        this.citas.detalles = "";
        this.citas.fecha = "";
        this.citas.lugar = "";
        this.citas.idContacto = 0;
      }
    });
  }

  public editar() {
    this.citaService.editarCita(this.citas)
    .subscribe(res => {
      this.toast.create({
        message: res.mensaje,
        duration: 2000
      }).present();
      if(res.estado) {
        this.navCtrl.push(CitasPage);
      } else {
       this.citas.asunto = "";
        this.citas.detalles = "";
        this.citas.fecha = "";
        this.citas.lugar = "";
        this.citas.idContacto = 0;
      }
    });
  }
  public eliminar() {
    this.citaService.eliminarCita(this.citas.idCita)
    .subscribe(res => {
      this.toast.create({
        message:res.mensaje,
        duration: 2000
      }).present();
      if(res.estado) {
        this.navCtrl.push(CitasPage);
      }
    });

  }

}
