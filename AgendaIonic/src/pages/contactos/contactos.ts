import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContactoService } from '../../app/services/contacto.service';
import { ContactoFormPage } from './contacto-form';

@Component({
  selector: 'page-contactos',
  templateUrl: 'contactos.html'
})
export class ContactosPage {
  private contactos:any[] = [];
  private c:string;
  constructor(
    public navCtrl: NavController,
    public contactoService: ContactoService
  ) {
    this.inicializar();
  }

  private inicializar() {
    this.contactoService.obtenerContactos()
    .subscribe(contactos => this.contactos = contactos);
    
    this.contactoService.obtenerContactos2()
    .subscribe(c => this.c = c);

  }
  public verForm(parametro) {
    this.navCtrl.push(ContactoFormPage, {parametro});
  }
}
