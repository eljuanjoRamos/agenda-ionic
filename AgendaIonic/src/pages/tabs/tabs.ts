import { Component } from '@angular/core';

import { ContactosPage } from '../contactos/contactos';
import { CitasPage } from '../citas/citas';
import { TareasPage } from '../tareas/tareas';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ContactosPage;
  tab3Root = CitasPage;
  tab4Root = TareasPage;
  constructor() {

  }
}
