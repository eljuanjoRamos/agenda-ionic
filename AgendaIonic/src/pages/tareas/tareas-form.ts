import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { TareaService } from '../../app/services/tarea.service';
import { TareasPage } from './tareas';

@Component({
  selector: 'page-tareas-form',
  templateUrl: 'tareas-form.html'
})
export class TareasFormPage {
  private tareas:any = {
    titulo: "",
    descripcion: "",
    fechaEntrega: "",
    esatdo: ""
  };
  private parametro:string;
  private encabezado:string;
  private texto:string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public tareaService: TareaService
  ) {
    this.parametro = this.navParams.get('parametro');
    if(this.parametro != 'nueva') {
      this.encabezado = "Detalle Tarea";
      this.texto = "Datos Actuales de la Tarea";
      this.tareaService.buscarTarea(this.parametro)
      .subscribe(tareas => this.tareas = tareas);
    } else {
      this.encabezado = "Nueva Tarea";
      this.texto = "Nuevos Datos";
    }
  }

  public guardar() {
    this.tareaService.nuevaTarea(this.tareas)
    .subscribe(res => {
      this.toast.create({
        message: res.mensaje,
        duration: 2000
      }).present();
      if(res.estado) {
        this.navCtrl.push(TareasPage);
      } else {
        this.tareas.titulo = "";
        this.tareas.descripcion = "";
        this.tareas.fechaEntrega = "";
        this.tareas.esatdo = "";
      }
    });
  }

  public editar() {
    this.tareaService.editarTarea(this.tareas)
    .subscribe(res => {
      this.toast.create({
        message: res.mensaje,
        duration: 2000
      }).present();
      if(res.estado) {
        this.navCtrl.push(TareasPage);
      } else {
        this.tareas.titulo = "";
        this.tareas.descripcion = "";
        this.tareas.fechaEntrega = "";
        this.tareas.esatdo = "";
      }
    });
  }
  public eliminar() {
    this.tareaService.eliminar(this.tareas.idTarea)
    .subscribe(res => {
      this.toast.create({
        message:res.mensaje,
        duration: 2000
      }).present();
      if(res.estado) {
        this.navCtrl.push(TareasPage);
      }
    });
  }

}
