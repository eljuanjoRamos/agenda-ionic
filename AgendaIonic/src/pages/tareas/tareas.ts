import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TareaService } from '../../app/services/tarea.service';
import { TareasFormPage } from './tareas-form';

@Component({
  selector: 'page-tareas',
  templateUrl: 'tareas.html'
})
export class TareasPage {
  private tareas:any[] = [];
  constructor(
    public navCtrl: NavController,
    public tareaService: TareaService
  ) {
    this.inicializar();
  }
  private inicializar() {
    this.tareaService.obtenerTareas()
    .subscribe(tareas => this.tareas = tareas);
  }
  public verForm(parametro) {
    this.navCtrl.push(TareasFormPage, {parametro});
  }
  
}
