import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../app/services/auth.service';
import { LoginPage } from '../login/login';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public auth:AuthService) {

  }
  public cerrarSesion() {
    let verificar = this.auth.salir();
    if(verificar) {
      this.navCtrl.push(LoginPage);
    } else {
      this.navCtrl.push(HomePage);
    }
  }

}
