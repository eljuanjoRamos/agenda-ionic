import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class AuthService {
  private headers:Headers;
  private url:string;
  private id:number;
  constructor(private http:Http) {
    this.url = "http://localhost:3000";
    let config = {
      'Content-Type': 'application/json'
    }
    this.headers = new Headers(config);
  }

  public autenticar(usuario:any) {
    let uri = `${this.url}/auth/`;
    let data = JSON.stringify(usuario);
    return this.http.post(uri, data, { headers:this.headers })
    .map(res => {
      if(res.json().token) {
        this.setId(res.json().idUsuario);
        this.setToken(res.json().token);
      }
      return res.json();
    });
  }
  public registrar(usuario:any) {
    let uri = `${this.url}/api/v1/usuario/`;
    let data = JSON.stringify(usuario);
    return this.http.post(uri, data, { headers:this.headers })
    .map(res => {
      if(res.json().token) {
        this.setToken(res.json().token);
        this.setId(res.json().idUsuario);
      }
      return res.json();
    });
  }
  private setId(id:any) {
    this.id = id;
  }
  public getId(): number {
    return this.id;
  }

  private setToken(token:string) {
    localStorage.setItem('TOKEN', token);
  }
  
  public getToken():string {
    return localStorage.getItem('TOKEN');
  }
  public salir(): boolean {
    localStorage.removeItem('TOKEN');
    return true;
  }
}
