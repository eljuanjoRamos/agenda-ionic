import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ContactoFormPage } from '../pages/contactos/contacto-form';
import { ContactosPage } from '../pages/contactos/contactos';
import { CitasPage } from '../pages/citas/citas';
import { CitasFormPage } from '../pages/citas/citas-form';
import { TareasPage } from '../pages/tareas/tareas';
import { TareasFormPage } from '../pages/tareas/tareas-form';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { SignUpPage } from '../pages/signup/signup';
import { AuthService } from './services/auth.service';
import { ContactoService } from './services/contacto.service';
import { TareaService} from './services/tarea.service';
import { CitaService } from './services/cita.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    ContactosPage,
    ContactoFormPage,
    CitasPage,
    CitasFormPage,
    TareasPage,
    TareasFormPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignUpPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactosPage,
    HomePage,
    ContactoFormPage,
    CitasPage,
    CitasFormPage,
    TareasPage,
    TareasFormPage,
    TabsPage,
    LoginPage,
    SignUpPage
  ],
  providers: [
    StatusBar,
    AuthService,
    ContactoService,
    CitaService,
    TareaService,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
